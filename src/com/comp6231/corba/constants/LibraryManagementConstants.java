package com.comp6231.corba.constants;

public class LibraryManagementConstants {
	
	
	public static String CONCORDIA_CODE = "CON";
	public static String MCGILL_CODE = "MCG";
	public static String MONTREAL_CODE = "MON";
	public static String USER_CODE = "U";
	public static String MANAGER_CODE = "M";
	public static String LOGGER_FOLDER="/logs";
	public static String CONCORDIA_INITAL_LOAD_FILE = "/resources/ConcordiaLibrary.txt";
	public static String MCGGILL_INITAL_LOAD_FILE = "/resources/McgillLibrary.txt";
	public static String MONTREAL_INITAL_LOAD_FILE = "/resources/MontrealLibrary.txt";
	public static String CONCORDIA_SERVER_LOG_FILE = "/log/concordia_server_log.log";
	public static String MCGILL_SERVER_LOG_FILE = "/log/mcgill_server_log.log";
	public static String MONTREAL_SERVER_LOG_FILE = "/log/montreal_server_log.log";
	public static String LOG_FOLDER = "/log/";
	public static String CONCORDIA_SERVER = "CONCORDIA_SERVER";
	public static String MCGILL_SERVER = "MCGILL_SERVER";
	public static String MONTREAL_SERVER = "MONTREAL_SERVER";
	public static String WAITING_LIST_MESSAGE="Book not available. Do you want to be added in waiting list?(y/n)";
	public static int CONCORDIA_SERVER_PORT = 5555;
	public static int MCGILL_SERVER_PORT = 6666;
	public static int MONTREAL_SERVER_PORT = 7777;
	public static String TRUE = "true";
	public static String FALSE = "false";

}
