package com.comp6231.corba.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import com.comp6231.corba.constants.LibraryManagementConstants;
import com.comp6231.corba.impl.McgillLibraryImpl;

import LibraryManagementSystemApplication.LibraryManagementSystem;
import LibraryManagementSystemApplication.LibraryManagementSystemHelper;

public class McgillLibraryServer {

	public static void main(String[] args) {
		McgillLibraryImpl mcgillLibraryImpl = new McgillLibraryImpl();
		Runnable mcgillRequestServerRunnable = () ->{
			handleMcgillRequests(mcgillLibraryImpl, args);
		};
		
		Runnable libraryServerRunnable = () ->{
			handlesRequestFromAnotherServers(mcgillLibraryImpl);
		};
		new Thread(mcgillRequestServerRunnable).start();
		new Thread(libraryServerRunnable).start();
	}
	
	/**
	 * handlesRequestFromAnotherServers method to handle requests from the other servers
	 * @param registry 
	 * @param mcgillLibraryImpl 
	 * @throws IOException
	 */
	private static void handlesRequestFromAnotherServers(McgillLibraryImpl mcgillLibraryImpl){
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket(LibraryManagementConstants.MCGILL_SERVER_PORT);
			System.out.println("McGill Server started...");
			while(true) {
				byte [] message = new byte[1000];
				DatagramPacket recievedDatagramPacket = new DatagramPacket(message, message.length);
				socket.receive(recievedDatagramPacket);
				String response = mcgillLibraryImpl.handleRequestFromOtherServer(new String(recievedDatagramPacket.getData()));
				DatagramPacket reply = new DatagramPacket(response.getBytes(), response.length(), recievedDatagramPacket.getAddress(),
						recievedDatagramPacket.getPort());
				socket.send(reply);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(socket != null)
				socket.close();
		}
	}

	private static void handleMcgillRequests(McgillLibraryImpl mcgillLibraryImpl, String[] args) {

		ORB orb = ORB.init(args, null);
		try {
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			
			mcgillLibraryImpl.setOrb(orb);
			
			LibraryManagementSystem href = LibraryManagementSystemHelper.narrow(rootPoa.servant_to_reference(mcgillLibraryImpl));
			
			NamingContextExt namingContextReference = NamingContextExtHelper.narrow(orb.resolve_initial_references("NameService"));
			NameComponent[] path = namingContextReference.to_name(LibraryManagementConstants.MCGILL_SERVER);
			
			namingContextReference.rebind(path, href);
			System.out.println("Mcgill Server is ready");
			while(true) {
				orb.run();
			}
			
		} catch (InvalidName | AdapterInactive | org.omg.CosNaming.NamingContextPackage.InvalidName | ServantNotActive | WrongPolicy | NotFound | CannotProceed e) {
			System.out.println("Something went wrong in concordai server: "+e.getMessage());
		}
		
	
	}

}
