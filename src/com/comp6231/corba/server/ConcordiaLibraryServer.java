package com.comp6231.corba.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import com.comp6231.corba.constants.LibraryManagementConstants;
import com.comp6231.corba.impl.ConcordiaLibraryImpl;

import LibraryManagementSystemApplication.LibraryManagementSystem;
import LibraryManagementSystemApplication.LibraryManagementSystemHelper;

public class ConcordiaLibraryServer {

	public static void main(String[] args) {
		ConcordiaLibraryImpl concordiaImpl = new ConcordiaLibraryImpl();
		Runnable libraryServerRunnable = () ->{
			handlesRequestFromAnotherServers(concordiaImpl);
		};
		
		Runnable concordiaRequestServerRunnable = () ->{
			handleConcordiaRequests(concordiaImpl,args);
		};
		
		new Thread(libraryServerRunnable).start();
		new Thread(concordiaRequestServerRunnable).start();
	}
	
	private static void handlesRequestFromAnotherServers(ConcordiaLibraryImpl concordiaLibraryImpl){
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket(LibraryManagementConstants.CONCORDIA_SERVER_PORT);
			System.out.println("Concordia Server started...");
			while(true) {
				byte [] message = new byte[1000];
				DatagramPacket recievedDatagramPacket = new DatagramPacket(message, message.length);
				socket.receive(recievedDatagramPacket);
				String response = concordiaLibraryImpl.handleRequestFromOtherServer(new String(recievedDatagramPacket.getData()));
				DatagramPacket reply = new DatagramPacket(response.getBytes(), response.length(), recievedDatagramPacket.getAddress(),
						recievedDatagramPacket.getPort());
				socket.send(reply);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(socket != null)
				socket.close();
		}
	}

	private static void handleConcordiaRequests(ConcordiaLibraryImpl concordiaImpl, String[] args) {
		ORB orb = ORB.init(args, null);
		try {
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			
			concordiaImpl.setOrb(orb);
			
			LibraryManagementSystem href = LibraryManagementSystemHelper.narrow(rootPoa.servant_to_reference(concordiaImpl));
			
			NamingContextExt namingContextReference = NamingContextExtHelper.narrow(orb.resolve_initial_references("NameService"));
			NameComponent[] path = namingContextReference.to_name(LibraryManagementConstants.CONCORDIA_SERVER);
			
			namingContextReference.rebind(path, href);
			System.out.println("Concordia Server is ready");
			while(true) {
				orb.run();
			}
			
		} catch (InvalidName | AdapterInactive | org.omg.CosNaming.NamingContextPackage.InvalidName | ServantNotActive | WrongPolicy | NotFound | CannotProceed e) {
			System.out.println("Something went wrong in concordai server: "+e.getMessage());
		}
		
	}

}
