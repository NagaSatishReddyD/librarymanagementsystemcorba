package com.comp6231.corba.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.omg.CORBA.ORB;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import org.omg.PortableServer.POAManagerPackage.AdapterInactive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import com.comp6231.corba.constants.LibraryManagementConstants;
import com.comp6231.corba.impl.MontrealLibraryImpl;

import LibraryManagementSystemApplication.LibraryManagementSystem;
import LibraryManagementSystemApplication.LibraryManagementSystemHelper;

public class MontrealLibraryServer {

	public static void main(String[] args) {
		MontrealLibraryImpl montrealLibraryImpl = new MontrealLibraryImpl();
		Runnable montrealRequestServerRunnable = () ->{
			handleMontrealRequests(montrealLibraryImpl, args);
		};
		
		Runnable libraryServerRunnable = () ->{
				handlesRequestFromAnotherServers(montrealLibraryImpl);
		};
		new Thread(montrealRequestServerRunnable).start();
		new Thread(libraryServerRunnable).start();
	}

	private static void handleMontrealRequests(MontrealLibraryImpl montrealLibraryImpl, String[] args) {

		ORB orb = ORB.init(args, null);
		try {
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			
			montrealLibraryImpl.setOrb(orb);
			
			LibraryManagementSystem href = LibraryManagementSystemHelper.narrow(rootPoa.servant_to_reference(montrealLibraryImpl));
			
			NamingContextExt namingContextReference = NamingContextExtHelper.narrow(orb.resolve_initial_references("NameService"));
			NameComponent[] path = namingContextReference.to_name(LibraryManagementConstants.MONTREAL_SERVER);
			
			namingContextReference.rebind(path, href);
			System.out.println("Montreal Server is ready");
			while(true) {
				orb.run();
			}
			
		} catch (InvalidName | AdapterInactive | org.omg.CosNaming.NamingContextPackage.InvalidName | ServantNotActive | WrongPolicy | NotFound | CannotProceed e) {
			System.out.println("Something went wrong in concordai server: "+e.getMessage());
		}
	}
	
	private static void handlesRequestFromAnotherServers(MontrealLibraryImpl montrealLibraryImpl){
		DatagramSocket socket = null;
		try {
			socket = new DatagramSocket(LibraryManagementConstants.MONTREAL_SERVER_PORT);
			System.out.println("Montreal Server started...");
			while(true) {
				byte [] message = new byte[1000];
				DatagramPacket recievedDatagramPacket = new DatagramPacket(message, message.length);
				socket.receive(recievedDatagramPacket);
				String response = montrealLibraryImpl.handleRequestFromOtherServer(new String(recievedDatagramPacket.getData()));
				DatagramPacket reply = new DatagramPacket(response.getBytes(), response.length(), recievedDatagramPacket.getAddress(),
						recievedDatagramPacket.getPort());
				socket.send(reply);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(socket != null)
				socket.close();
		}
	}

}
