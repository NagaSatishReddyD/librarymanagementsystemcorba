package LibraryManagementSystemApplication;


/**
* LibraryManagementSystemApplication/LibraryManagementSystemOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from LibraryManagementSystem.idl
* Saturday, 23 February, 2019 9:45:08 AM EST
*/

public interface LibraryManagementSystemOperations 
{
  String addItem (String managerId, String itemId, String itemName, int quantity);
  String listItemAvailability (String managerId);
  String removeItem (String userId, String itemId, int quantity);
  String borrowItem (String userId, String itemId);
  String addToWaitingList (String userId, String itemId);
  String findItem (String userId, String itemName, boolean fromOtherServer);
  String returnItem (String userId, String itemId);
  String exchangeItem (String userId, String newItemId, String oldItemId);
  void shutdown ();
} // interface LibraryManagementSystemOperations
